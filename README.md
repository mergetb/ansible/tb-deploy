# tb-deploy

A one-stop-shop testbed deployment playbook

## Usage

This playbook is designed to be used with the
[merge-confgen](https://gitlab.com/mergetb/tech/cogs/-/tree/tbmodeling/util/merge-confgen)
tool. The general workflow is the following, where `<tbxir.json>` is the path to
the XIR model of your testbed. `merge-confgen` will generate the
`tb-deploy-vars.json` file.

```shell
merge-confgen ansible tb-deploy <tbxir.json>
ansible-playbook -e "@tb-deploy-vars.json" -e "xir=<tbxir.json>" main.yml
```
