#!/bin/bash

set -e
set -o pipefail

latest=`ls -1 /boot/vmlinuz* | sort -r | head -1 | grep -oP '[4,5].[0-9][0-9]?.[0-9][0-9]?.*'`
current=`uname -r`

if [[ $current != $latest ]]; then
	echo "needs reboot"
	exit 47
fi

